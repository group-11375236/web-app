import React, { useRef, useState } from "react";
import { useAuth } from "../context/AuthContext";
import { Alert } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

import "../assets/css/login.css"

export default function Login() {

  const emailRef = useRef()
  const passwordRef = useRef()
  const { signin } = useAuth()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate();

  async function handleSubmit(e) {
    e.preventDefault()
    
    try {
      setError("")
      setLoading(true)
      await signin(emailRef.current.value, passwordRef.current.value)
      navigate("/user/home")
    } catch {
      setError("Credentials not found.")
    }
    setLoading(false)
  }

  return (
    <div className="login-body">
      <div className="login-container">
        <div className="forms">
          <div className="login-form">
            <span className="title">Login</span>
            <form onSubmit={handleSubmit}>
              <div className="input-field">
                <input 
                type="email" 
                placeholder="Email" 
                required
                ref={emailRef} />
                <img src="/svg/email.svg" />
              </div>
              <div className="input-field">
                <input
                  type="password"
                  className="password"
                  placeholder="Password"
                  required
                  ref={passwordRef}
                />
                <img src="/svg/lock.svg" />
              </div>

              <div className="alert">
              {error && <Alert variant="danger">{error}</Alert>}
              </div>

              <div className="button">
                <button disabled={loading}>Login</button>
              </div>
            </form>
            <div className="login-signup">
              <span className="text">
                Not a member?
                <Link to="register" className="text signup-link">
                  Register
                </Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
