import React, { useState, useEffect, useRef } from 'react';
import { firestore, auth} from '../firebase';
import '../assets/css/Chat.css';
import emojiList from '../assets/js/emojiList';

const Chat = () => {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState(null);
  const messageListRef = useRef(null);

  useEffect(() => {
    const unsubscribe = firestore.collection('messages')
      .orderBy('timestamp', 'asc') 
      .onSnapshot((snapshot) => {
        const messageData = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
        setMessages(messageData);
      });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  const scrollToBottom = () => {
    if (messageListRef.current) {
      messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
    }
  };

  const handleSendMessage = async () => {
  if (newMessage.trim() === '') return;

  try {
    const user = auth.currentUser; 
    const userData = await firestore.collection('users').doc(user.uid).get();
    const displayName = userData.get('displayName'); 
    const photoURL = userData.get('photoURL'); 

    await firestore.collection('messages').add({
      text: newMessage,
      timestamp: new Date(),
      displayName, 
      photoURL, 
    });
    setNewMessage('');
  } catch (error) {
    console.error('Error sending message: ', error);
  }
};

  const handleInputKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleSendMessage();
    }
  };

  const handleEmojiSelect = (emoji) => {
    setNewMessage(newMessage + emoji);
  };

  const handleMessageClick = (message) => {
    setSelectedMessage(message);
  };

  const handleEmojiPickerToggle = () => {
    setShowEmojiPicker(!showEmojiPicker);
  };

  return (
    <div className="chat-container">
      <h1 className="title-1">Consultation</h1>
      <h2 className="title-2">Consultation</h2>
      <div className="message-list" ref={messageListRef}>
        {messages.map((message) => (
          <div key={message.id} className={`message ${message.sentByCurrentUser ? 'sent' : 'received'}`} onClick={() => handleMessageClick(message)}>
            <div className="message-header">
              <img src={message.photoURL} alt={`${message.displayName}'s profile`} className="message-avatar" />
              <div className="message-sender">{message.displayName}</div>
            </div>
            <div className="message-text">
              {message.text}
            </div>
            {selectedMessage && selectedMessage.id === message.id && (
              <div className="message-details">
                {message.timestamp.toDate().toLocaleString()}
              </div>
            )}
          </div>
        ))}
      </div>
      <div className="chat-input">
        <button onClick={handleEmojiPickerToggle} className="emoji-button">
          🙂
        </button>
        {showEmojiPicker && (
          <div className="emoji-picker">
            <div className="emoji-picker-inner">
              {emojiList.map((emoji) => (
                <span key={emoji} onClick={() => handleEmojiSelect(emoji)}>
                  {emoji}
                </span>
              ))}
            </div>
          </div>
        )}
        <input
          type="text"
          placeholder="Type a message"
          value={newMessage}
          onChange={(e) => setNewMessage(e.target.value)}
          onKeyDown={handleInputKeyDown}
        />
        <button onClick={handleSendMessage}>Send</button>
      </div>
    </div>
  );
};

export default Chat;