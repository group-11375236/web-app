import React, { useState } from 'react';
import '../assets/css/SurveyForm.css';
import symptoms from '../assets/js/symptoms';
import symptomToIssueMapping from '../assets/js/symptomToIssueMapping.js';
import SecurityForm from './SecurityForm';

const SurveyForm = () => {
  const [selectedSymptoms, setSelectedSymptoms] = useState([]);
  const [potentialIssues, setPotentialIssues] = useState([]);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [securityFormDetails, setSecurityFormDetails] = useState(null);

  const handleDetailsSubmit = (details) => {
    setSecurityFormDetails(details);
  };

  const handleSymptomChange = (symptom) => {
    const updatedSymptoms = selectedSymptoms.includes(symptom)
      ? selectedSymptoms.filter((s) => s !== symptom)
      : [...selectedSymptoms, symptom];
    setSelectedSymptoms(updatedSymptoms);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!securityFormDetails) {
      console.error('Please submit security form details first.');
      return;
    }

    const potentialIssues = getPotentialIssues(selectedSymptoms);
    setPotentialIssues(potentialIssues);

    try {
      const response = await fetch('http://localhost:2000/api/data', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          ...securityFormDetails,
          symptoms: selectedSymptoms,
          potentialIssues,
        }),
      });

      if (response.ok) {
        console.log('User data submitted successfully');
        setIsPopupOpen(true);
      } else {
        console.error('Error submitting user data:', response.statusText);
      }
    } catch (error) {
      console.error('Error submitting user data:', error);
    }
  };

  const handleReturnToForm = () => {
    setIsPopupOpen(false);
    setSelectedSymptoms([]);
    setPotentialIssues([]);
    setSecurityFormDetails(null);
  };

  const getPotentialIssues = (selectedSymptoms) => {
    const potentialIssues = selectedSymptoms.reduce((issues, symptom) => {
      if (symptomToIssueMapping[symptom]) {
        issues.push(...symptomToIssueMapping[symptom]);
      }
      return issues;
    }, []);

    return [...new Set(potentialIssues)];
  };

  return (
    <div className="survey-container">
      {securityFormDetails ? (
        <form onSubmit={handleSubmit} className="survey-form">
          <h2>Select everything you are feeling now:</h2>
          {symptoms.map((symptom) => (
            <label key={symptom} className="survey-label">
              <input
                type="checkbox"
                checked={selectedSymptoms.includes(symptom)}
                onChange={() => handleSymptomChange(symptom)}
              />
              {symptom}
            </label>
          ))}
          <div className="survey-btn">
          <button type="submit">Submit</button>
          </div>
          
        </form>
      ) : (
        <SecurityForm onDetailsSubmit={handleDetailsSubmit} />
      )}
      {isPopupOpen && (
        <div className="popup">
          <div className="popup-content">
            <span className="close" onClick={handleReturnToForm}>
              &times;
            </span>
            <h2>Potential Issues:</h2>
            <ul>
              {potentialIssues.map((issue, index) => (
                <li key={index}>{issue}</li>
              ))}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default SurveyForm;

