import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const app = firebase.initializeApp({
  apiKey: "AIzaSyA6dPfUvmlfuuxyF6GCJ5X055qs_R0Dkl4",
  authDomain: "communicare-auth-c4a15.firebaseapp.com",
  databaseURL: "https://communicare-auth-c4a15-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "communicare-auth-c4a15",
  storageBucket: "communicare-auth-c4a15.appspot.com",
  messagingSenderId: "701515357238",
  appId: "1:701515357238:web:dbb8fe6d7e85fa47afcc97"
})

export const auth = app.auth();
export const firestore = app.firestore();
export const storage = app.storage();
export default app