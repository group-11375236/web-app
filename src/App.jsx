import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";


import RootLayout from "./Layout/RootLayout";


import Login from "./pages/Login"
import Register from "./pages/Register";
import Home from "./pages/Home"
import Chat from "./pages/Chat";
import Faqs from "./pages/Faqs";
import SurveyForm from "./pages/SurveyForm";
import Profile from "./pages/Profile";

const router = createBrowserRouter(
  createRoutesFromElements (
    <Route path="/">
      <Route index element={<Login />} />
      <Route path="register" element={<Register />} />
      <Route path="user" element={<RootLayout />} >
        <Route path="home" element={<Home />} />
        <Route path="survey form" element={<SurveyForm />} />
        <Route
          path="chat"
          element={<Chat />}
        />
        <Route path="faqs" element={<Faqs />} />
        <Route path="profile" element={<Profile />} />
      </Route>
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;