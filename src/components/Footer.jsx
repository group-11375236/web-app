import React from "react";

export default function Footer() {
  return (
    <footer>
      {/* <figure>
        <img src="/svg/campus-logo.svg" alt="school-logo" />
      </figure> */}
      <p>© 2023 CommuniCare. All Rights Reserved.</p>
    </footer>
  );
}
