import React from "react";
import { Link } from "react-router-dom";
import "../assets/css/logo.css";

export default function Profile({ user }) {
  return (
    <>
      <div className="profile">
        <Link to="/student/profile" className="profile-link">
          <div className="container">
            <img src="/images/logo.png" alt="Profile" />
            <span>CommuniCare</span>
            <p className="email">{user?.email}</p>
          </div>
        </Link>
        <div className="line"></div>
      </div>
    </>
  );
}
